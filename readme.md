# Ambiente de desarrollo local

Este proyecto consta de una serie de scripts para levantar un servidor virtual con 
configuración similar a la productiva para los clientes, proyectos o sistemas satélite. Está pensado para
ser usado como un servidor privado por cada uno de los desarrolladores. 

# Atención Desarrolladores

Todos los desarrolladores deben trabajar homologados, por lo tanto solo está permitido trabajar con sistema operativo windows 10, y el antivirus Eset Internet Security.

## Requerimientos

Para usar este proyecto es necesario tener instalado lo siguiente:

* VIRTUAL BOX
* VAGRANT
* GIT
* VISUAL STUDIO CODE

## Configuración
Para hacer accesibles las páginas publicadas por el servidor virtual, necesitamos poner 
las IP's y los nombres de dominio en el archivo hosts (/etc/hosts en linux y mac, 
C:\windows\system32\drivers\etc\hosts para windows )

	La información de IP's y Dominios se descarga de: https://oblak.dev/download/hosts.php

## Modo de uso
Una vez que tenemos la configuración correcta podemos manipular el servidor virtual desde la línea de comandos, con los siguientes comandos:

### Iniciar el servidor:
    
	vagrant  up
    
La primera vez que se usa este comando, vagrant genera y configura el servidor virtual,
por lo que es probable que se tarde varios minutos.   

### Detener el servidor:

	vagrant halt 

### Aplicar cambios en los archivos de configuración

	vagrant reload
	
Esto se hará cada vez que hagamos una corrección en los scripts de este proyecto para
que los servidores de cada uno tomen los cambios, obvio habrá que hacer un "git pull"  previamente. 
  
### Destruir la máquina

	vagrant destroy
	
Esta instrucción destruye el servidor virtual y debe ser usada con cuidado, 
es útil en el caso de que se desee liberar espacio en disco, cuando ya no se ocupe más
el servidor, o en caso de querer iniciar con un servidor limpio.
	
Los datos en las carpetas de los proyectos no se ven afectados.
	
Si después de ejecutar este comando ejecutamos una vez más vagrant up, 
la máquina se volverá a generar desde cero.

### Ingresar por ssh al servidor

	vagrant ssh
	
Este comando es muy útil para revisar logs del servidor, hacer cambios manuales a la configuración, reiniciar servicios, etc. 
	
Recuerda que cualquier configuración especial que hagas a mano aquí
diferirá de la configuración que tienen los demás y se perderá si ejecutas 
vagrant destroy	o en algunos casos si ejecutas vagrant reload,
si deseas hacer una configuración que se pueda regenerar automáticamente, 
hay que hacerlo en los scripts de este proyecto en sh.

### PROCEDIMIENTO PASO A PASO

1. Clonar repositorio de ambientes, en raíz del disco duro ejecutar el siguiente comando:
    * git clone https://oblakadmin@bitbucket.org/oblakdev/enviroment.git

2. Instalar el plugin vbguest, ejecutar el siguiente comando:
    * vagrant plugin install vagrant-vbguest

3. Iniciar el ambiente de desarrollo en vagrant, para esto deberemos dirigirnos al correspondiente, ejemplo:
    * D:\env\oblakdc.
    * Y ejecutaremos el comando: vagrant init.
	* Se creará la carpeta .vagrant

4. Crear máquina virtual del ambiente de desarrollo, ejecutar el siguiente comando:

	* vagrant up (se inicia un largo proceso)

5. Apagar Ambiente, cuando termina la instalación ejecutar el siguiente comando:

	* vagrant halt

6. Prender Ambiente, deberá ejecutar el siguiente comando:

    * vagrant reload

7. Acceso SSH, deberá ejecutar el siguiente comando:

    * vagrant ssh

8. Permisos root, deberá ejecutar el siguiente comando:

    * sudo su

9. Instalar panel de control:

    * yum -y  install wget && wget -O install.sh http://www.aapanel.com/script/install_6.0_en.sh && bash install.sh


Cualquier duda o problema, puedes contactar al departamento de tecnología.